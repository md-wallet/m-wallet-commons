package com.mwallet.mwalletcommons.parking;


import com.mwallet.mwalletcommons.utils.AMQPUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.test.binder.MessageCollector;
import org.springframework.context.annotation.Import;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.test.context.ActiveProfiles;

import java.util.HashMap;
import java.util.concurrent.BlockingQueue;


import static com.mwallet.mwalletcommons.utils.ObjectUtils.readJson;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ActiveProfiles("test")
@Import(ParkingTestConfiguration.class)
class ParkingListenerTest {

    @Autowired
    private ParkingSink parkingSink;

    @Autowired
    private MessageCollector messageCollector;

    @Autowired
    private ParkingTestConfiguration.TestOutput testOutput;

    @Test
    void receiveParkedMessage() {

        //GIVEN object
        HashMap<String, String> msg = new HashMap<>(){{
           put("1","hello");
           put("2","world");
        }};

        //GIVEN headers
        HashMap<String, Object> headers = new HashMap<>(){{
            put("x-return","testChannel");
        }};

        //WHEN
        parkingSink.parkingInput().send(AMQPUtils.buildMessageFrom(msg,headers));

        //THEN
        BlockingQueue<Message<?>> blockingQueue = messageCollector.forChannel(testOutput.testChannelInput());
        Message<?> message =  blockingQueue.poll();
        assert message != null;
        HashMap<String,String> result = readJson(message.getPayload().toString(),HashMap.class);
        assertThat(result)
                .isNotNull()
                .isEqualTo(msg);
    }
}

@TestConfiguration
@EnableBinding(ParkingTestConfiguration.TestOutput.class)
class ParkingTestConfiguration{

    public interface TestOutput {
        String TEST_OUTPUT = "testChannel";
        @Output(TestOutput.TEST_OUTPUT)
        MessageChannel testChannelInput();
    }

}

