package com.mwallet.mwalletcommons.utils;

import org.assertj.core.data.Offset;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.assertj.core.api.Assertions.assertThat;

class AMQPUtilsTest {

    @ParameterizedTest
    @CsvSource({
            "1,1024,0",
            "2,1024,2",
            "8,1024,128",
            "14,1024,1024"
    })
    void exponentialDelay(long failedAttempts,long maxDelay,long expectedDelay) {
        long actualDelay = AMQPUtils.exponentialDelay(failedAttempts,maxDelay);
        assertThat(actualDelay).isCloseTo(expectedDelay,Offset.offset(1L));
    }
}