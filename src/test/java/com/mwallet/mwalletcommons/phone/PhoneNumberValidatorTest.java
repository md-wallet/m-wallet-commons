package com.mwallet.mwalletcommons.phone;

import com.mwallet.mwalletcommons.annotations.phone.PhoneNumber;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("test")
@SpringBootTest
class PhoneNumberValidatorTest {

    @Autowired
    Validator myValidator;

    @ParameterizedTest
    @MethodSource("phoneNumbersSource")
    void isValid(TestUsage givenTestUsage, int expectedViolations) {
        //WHEN
        Set<ConstraintViolation<TestUsage>> constraintViolations = myValidator.validate(givenTestUsage);
        //THEN
        assertThat(constraintViolations).hasSize(expectedViolations);
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    static class TestUsage {
        @PhoneNumber
        String phoneNumber;

        @PhoneNumber
        List<String> phoneNumbers;
    }

    static Stream<Arguments> phoneNumbersSource() {
        return Stream.of(
                Arguments.of(new TestUsage("+254708029879", List.of("+254708029879")), 0),
                Arguments.of(new TestUsage("+551155256325", List.of("+551155256325")), 0),
                Arguments.of(new TestUsage("+14155552671", List.of("+14155552671")), 0),
                Arguments.of(new TestUsage("+1452671", List.of("+1452671")), 2),
                Arguments.of(new TestUsage("254708029879", List.of("254708029879")), 2));
    }
}