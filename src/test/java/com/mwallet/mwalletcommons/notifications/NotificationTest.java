package com.mwallet.mwalletcommons.notifications;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.List;


import static com.mwallet.mwalletcommons.utils.ObjectUtils.readJson;
import static com.mwallet.mwalletcommons.utils.ObjectUtils.writeJson;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
class NotificationTest {


    private final HashMap<Notification.Link, List<EgressType>> recipientEgressTypes = new HashMap<>();

    @BeforeEach
    void setUp() {
        recipientEgressTypes.put(Notification.Link.builder().linkId("TEST").linkType(Notification.LinkType.USER).build(),List.of(EgressType.SMS));
    }

    @SuppressWarnings("unchecked")
    @Test
    void checkSerializationAndDeserialization(){
        String map = writeJson(recipientEgressTypes);
        HashMap<Notification.Link, List<EgressType>> recipientEgressTypes = readJson(map,HashMap.class);

        assertThat(recipientEgressTypes)
                .isNotNull();

    }

}