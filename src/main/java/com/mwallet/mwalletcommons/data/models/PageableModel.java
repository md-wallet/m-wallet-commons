package com.mwallet.mwalletcommons.data.models;

import lombok.*;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author lawrence
 * created 04/02/2020 at 22:13
 **/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class PageableModel<T> {
    private Integer page;
    private Integer rows;
    private Long totalRows;
    private Integer totalPages;
    private List<T> items;

    public static <T> PageableModel<T> from(Page<T> page) {
        return PageableModel.<T>builder()
                .totalRows(page.getTotalElements())
                .items(page.getContent())
                .totalPages(page.getTotalPages())
                .page(page.getNumber())
                .rows(page.getNumberOfElements())
                .build();
    }

    public <S> PageableModel<S> map(Function<? super T, ? extends S> converter) {
        List<S> items = this.items.stream().map(converter).collect(Collectors.toList());
        return PageableModel.<S>builder()
                .totalRows(this.totalRows)
                .rows(this.rows)
                .page(this.page)
                .totalPages(this.totalPages)
                .items(items)
                .build();
    }
}
