package com.mwallet.mwalletcommons.data.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

/**
 * @author lawrencemwaniki
 * created 08/04/2020 at 10:42
 **/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder(toBuilder = true)
public class BaseDTO {

    @ApiModelProperty(required = true, example = "2019-10-11T08:00:00Z")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "Africa/Nairobi")
    private LocalDateTime createdOn;

    @ApiModelProperty(required = true, example = "2019-10-11T08:00:00Z")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "Africa/Nairobi")
    private LocalDateTime updatedOn;
}
