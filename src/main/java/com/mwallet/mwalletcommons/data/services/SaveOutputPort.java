package com.mwallet.mwalletcommons.data.services;



import com.mwallet.mwalletcommons.annotations.OutputPort;
import com.mwallet.mwalletcommons.data.models.BaseDomain;
import com.mwallet.mwalletcommons.exceptions.GenericHttpException;

import java.util.List;
import java.util.function.Supplier;

/**
 * @author lawrencemwaniki
 * created 26/04/2020 at 18:26
 **/
@OutputPort
interface SaveOutputPort<D extends BaseDomain> {

    D create(D domainModel);

    List<D> create(List<D> domainModels);

    <R extends GenericHttpException> D update(D domainModel) throws R;

    List<D> update(List<D> domainModels);

    void delete(D domainModel);

    <R extends GenericHttpException> Supplier<R> resourceNotFoundExceptionSupplier();
}
