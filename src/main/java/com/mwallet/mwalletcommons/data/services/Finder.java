package com.mwallet.mwalletcommons.data.services;

import com.mwallet.mwalletcommons.data.models.BaseDomain;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

/**
 * @author lawrencemwaniki
 * created 26/04/2020 at 18:50
 **/
@SuppressWarnings({"unchecked"})
public class Finder<DOMAIN_MODEL extends BaseDomain> {

    protected final Map<Object, BiConsumer<? super DOMAIN_MODEL, Object>> backingMap = new HashMap<>();
    Pageable pageable;

    //======================= initializer ========================
    public static <CONSTRAINT, DOMAIN_MODEL extends BaseDomain> Finder<DOMAIN_MODEL> by(CONSTRAINT value, BiConsumer<? super DOMAIN_MODEL, CONSTRAINT> setter) {
        Finder<DOMAIN_MODEL> finder = new Finder<>();
        return finder.and(value, setter);
    }

    public static <CONSTRAINT extends BaseDomain, DOMAIN_MODEL extends BaseDomain> Finder<DOMAIN_MODEL> by(Nested<DOMAIN_MODEL, CONSTRAINT> nested) {
        Finder<DOMAIN_MODEL> finder = new Finder<>();
        return finder.and(nested);
    }

    //======================= chain -> and ========================
    public <CONSTRAINT> Finder<DOMAIN_MODEL> and(CONSTRAINT value, BiConsumer<? super DOMAIN_MODEL, CONSTRAINT> setter) {
        backingMap.put(value, (BiConsumer<? super DOMAIN_MODEL, Object>) setter);
        return this;
    }

    public <T extends BaseDomain> Finder<DOMAIN_MODEL> and(Nested<DOMAIN_MODEL, T> nested) {
        return and(nested.nestedValue, nested.parentSetter);
    }

    //======================= filter -> pageable ========================
    public Finder<DOMAIN_MODEL> filter(Integer page, Integer size) {
        pageable = PageRequest.of(page, size, Sort.by("createdOn").descending());
        return this;
    }

    public Finder<DOMAIN_MODEL> filter(Pageable pageable) {
        this.pageable = pageable;
        return this;
    }

}
