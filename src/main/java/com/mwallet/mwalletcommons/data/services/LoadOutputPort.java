package com.mwallet.mwalletcommons.data.services;


import com.mwallet.mwalletcommons.annotations.OutputPort;
import com.mwallet.mwalletcommons.data.models.BaseDomain;
import com.mwallet.mwalletcommons.data.models.PageableModel;
import com.mwallet.mwalletcommons.exceptions.GenericHttpException;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * @author lawrencemwaniki
 * created 26/04/2020 at 18:26
 **/
@OutputPort
interface LoadOutputPort<D extends BaseDomain> {

    boolean existsBy(Finder<D> finder);

    Optional<D> findOptionalBy(Finder<D> finder);

    <R extends GenericHttpException> D findBy(Finder<D> finder) throws R;

    Page<D> findPageBy(Finder<D> finder);

    List<D> findListBy(Finder<D> finder);

    PageableModel<D> findAllBy(Finder<D> finder);

    <R extends GenericHttpException> Supplier<R> resourceNotFoundExceptionSupplier();
}
