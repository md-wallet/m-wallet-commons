package com.mwallet.mwalletcommons.data.services;

import com.mwallet.mwalletcommons.data.models.BaseDomain;
import lombok.SneakyThrows;

import java.util.function.BiConsumer;
import java.util.function.Supplier;

/**
 * @author lawrencemwaniki
 * created 27/04/2020 at 14:21
 **/
public class Nested<PARENT extends BaseDomain, NESTED extends BaseDomain> {

    BiConsumer<? super PARENT, NESTED> parentSetter;
    NESTED nestedValue;

    private Nested() {
    }

    public static <PARENT extends BaseDomain, NESTED extends BaseDomain> Nested<PARENT, NESTED> as(Supplier<NESTED> nestedSupplier, BiConsumer<? super PARENT, NESTED> parentSetter) {
        Nested<PARENT, NESTED> nested = new Nested<>();
        nested.parentSetter = parentSetter;
        nested.nestedValue = nestedSupplier.get();
        return nested;
    }

    @SneakyThrows
    public <CONSTRAINT> Nested<PARENT, NESTED> with(CONSTRAINT value, BiConsumer<? super NESTED, CONSTRAINT> setter) {
        setter.accept(this.nestedValue, value);
        return this;
    }

    public <CONSTRAINT extends BaseDomain> Nested<PARENT, NESTED> with(Nested<NESTED, CONSTRAINT> nested) {
        nested.parentSetter.accept(this.nestedValue, nested.nestedValue);
        return this;
    }

}
