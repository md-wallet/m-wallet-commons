package com.mwallet.mwalletcommons.data.services;


import com.google.common.reflect.TypeToken;
import com.mwallet.mwalletcommons.data.models.BaseDomain;
import com.mwallet.mwalletcommons.data.models.BaseEntity;
import com.mwallet.mwalletcommons.data.models.PageableModel;
import com.mwallet.mwalletcommons.exceptions.GenericHttpException;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.mwallet.mwalletcommons.utils.ObjectUtils.writeJson;


/**
 * @author lawrencemwaniki
 * created 26/04/2020 at 18:30
 **/

@SuppressWarnings({"unchecked", "UnstableApiUsage"})
@Slf4j
public abstract class GenericPersistenceAdapter<D extends BaseDomain, E extends BaseEntity> implements SaveOutputPort<D>, LoadOutputPort<D> {

    private final TypeToken<D> domainTypeToken = new TypeToken<>(getClass()) {
    };
    private final TypeToken<E> entityTypeToken = new TypeToken<>(getClass()) {
    };

    protected final ModelMapper modelMapper;
    private final JpaRepository<E, Long> jpaRepository;

    protected GenericPersistenceAdapter(ModelMapper modelMapper, JpaRepository<E, Long> jpaRepository) {
        this.modelMapper = modelMapper;
        this.jpaRepository = jpaRepository;
    }

    @Override
    public D create(D domainModel) {
        //1. map to entity
        E mappedEntity = modelMapper.map(domainModel, entityTypeToken.getType());

        //2. save
        E savedEntity = jpaRepository.saveAndFlush(mappedEntity);

        //3. log
        log.debug("saved" + entityTypeToken.getType().getTypeName() + ": {}", writeJson(savedEntity));

        //4. map & return
        return modelMapper.map(savedEntity, domainTypeToken.getType());
    }

    @Override
    public List<D> create(List<D> domainModels) {
        //1. ensure entities dont exist
        domainModels.forEach(d -> {
            assert Objects.isNull(d.getId()) && Objects.isNull(d.getVersion()) : "Can only create non-existent entity";
        });

        //map to entity
        List<E> entityList = domainModels.stream().map(d -> (E) modelMapper.map(d, entityTypeToken.getType())).collect(Collectors.toList());

        //3.save
        List<E> createdEntities = jpaRepository.saveAll(entityList);

        //4.map
        log.debug("createdEntities: {}", createdEntities.size());
        return createdEntities.stream().map(entity -> (D) modelMapper.map(entity, domainTypeToken.getType())).collect(Collectors.toList());
    }

    @Override
    public <R extends GenericHttpException> D update(D domainModel) throws R {
        //1. check if exists
        if (jpaRepository.existsById(domainModel.getId())) resourceNotFoundExceptionSupplier().get();

        //map model to entity
        E mappedEntity = modelMapper.map(domainModel, entityTypeToken.getType());

        //3. update entity
        E updatedEntity = jpaRepository.saveAndFlush(mappedEntity);
        log.debug("updated" + entityTypeToken.getType().getTypeName() + ": {}", writeJson(updatedEntity));

        //4. map & return
        return modelMapper.map(updatedEntity, domainTypeToken.getType());
    }

    @Override
    public List<D> update(List<D> domainModels) {
        //1. ensure entities exist to avoid inserts
        domainModels.forEach(d -> {
            assert Objects.nonNull(d.getId()) && Objects.nonNull(d.getVersion()) : "Can only update existing entities";
        });

        //2. map to entities
        //map to entity
        List<E> entityList = domainModels.stream().map(d -> (E) modelMapper.map(d, entityTypeToken.getType())).collect(Collectors.toList());

        //3. save
        List<E> updatedEntities = jpaRepository.saveAll(entityList);

        //4. map
        log.debug("updatedEntities: {}", updatedEntities.size());
        return updatedEntities.stream().map(entity -> (D) modelMapper.map(entity, domainTypeToken.getType())).collect(Collectors.toList());
    }

    @SneakyThrows
    @Override
    public boolean existsBy(Finder<D> finder) {
        //1. create instance & apply setters
        D d = (D) domainTypeToken.getRawType().getConstructor().newInstance();
        finder.backingMap.forEach((key, value) -> value.accept(d, key));

        //2. map to entity & create example
        Example<E> example = Example.of(modelMapper.map(d, entityTypeToken.getType()));

        //3. find
        return jpaRepository.exists(example);
    }

    @Override
    public void delete(D domainModel) {
        jpaRepository.deleteById(domainModel.getId());
    }

    @SneakyThrows
    @Override
    public Optional<D> findOptionalBy(Finder<D> finder) {
        //1. create instance & apply setters
        D d = (D) domainTypeToken.getRawType().getConstructor().newInstance();
        finder.backingMap.forEach((key, value) -> value.accept(d, key));

        //2. map to entity & create example
        Example<E> example = Example.of(modelMapper.map(d, entityTypeToken.getType()));

        //find one
        return jpaRepository.findOne(example).map(e -> modelMapper.map(e, domainTypeToken.getType()));
    }

    @Override
    public <R extends GenericHttpException> D findBy(Finder<D> finder) throws R {
        return findOptionalBy(finder).orElseThrow(resourceNotFoundExceptionSupplier());
    }

    @SneakyThrows
    @Override
    public Page<D> findPageBy(Finder<D> finder) {
        assert Objects.nonNull(finder.pageable) : "Pageable details required!";

        //1. create instance & apply setters
        D d = (D) domainTypeToken.getRawType().getConstructor().newInstance();
        finder.backingMap.forEach((key, value) -> value.accept(d, key));

        //2. map to entity & create example
        Example<E> example = Example.of(modelMapper.map(d, entityTypeToken.getType()));

        //3. findAll
        return jpaRepository.findAll(example, finder.pageable).map(e -> modelMapper.map(e, domainTypeToken.getType()));
    }

    @Override
    public List<D> findListBy(Finder<D> finder) {
        return findPageBy(finder.filter(Pageable.unpaged())).getContent();
    }

    @Override
    public PageableModel<D> findAllBy(Finder<D> finder) {
        return PageableModel.from(findPageBy(finder));
    }
}
