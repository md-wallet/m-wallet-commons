package com.mwallet.mwalletcommons.annotations;

import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.lang.annotation.*;

/**
 * @author lawrence
 * created 19/11/2019 at 15:40
 **/

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Service
@Validated
public @interface PersistenceAdapter {

    @AliasFor(annotation = Service.class)
    String value() default "";

}
