package com.mwallet.mwalletcommons.annotations;

import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import java.lang.annotation.*;

/**
 * @author lawrence
 * created 19/11/2019 at 15:42
 **/

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
@Validated
public @interface InputPort {
    @AliasFor(annotation = Component.class)
    String value() default "";

}
