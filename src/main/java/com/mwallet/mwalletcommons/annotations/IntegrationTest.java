package com.mwallet.mwalletcommons.annotations;


import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.annotation.AliasFor;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@ActiveProfiles("test")
public @interface IntegrationTest {

    @AliasFor(annotation = SpringBootTest.class)
    String[] value() default {};

    @AliasFor(annotation = SpringBootTest.class)
    String[] properties() default {};

    @AliasFor(annotation = SpringBootTest.class)
    String[] args() default {};

    @AliasFor(annotation = SpringBootTest.class)
    Class<?>[] classes() default {};
}
