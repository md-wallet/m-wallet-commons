package com.mwallet.mwalletcommons.annotations;

import com.mwallet.mwalletcommons.exceptions.GlobalExceptionHandlerController;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author lawrence
 * created 27/11/2019 at 13:13
 **/
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
@ComponentScan(basePackages = {"com.mwallet.commons"})
@Import(GlobalExceptionHandlerController.class)
public @interface EnableMWalletCommons {
}
