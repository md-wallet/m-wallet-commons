package com.mwallet.mwalletcommons.annotations;

import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * @author lawrence
 * created 22/11/2019 at 07:08
 **/
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface TypeMapper {
    @AliasFor(annotation = Component.class)
    String value() default "";
}
