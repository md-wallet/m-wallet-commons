package com.mwallet.mwalletcommons.annotations.phone;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import lombok.extern.slf4j.Slf4j;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;

import static com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberType.MOBILE;

@Slf4j
class PhoneNumberValidator {

    static class FieldValidator extends AbstractPhoneValidator<String> {
        @Override
        public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
            return isValidPhoneNumber(s);
        }
    }

    static class ListValidator extends AbstractPhoneValidator<List<String>> {
        @Override
        public boolean isValid(List<String> strings, ConstraintValidatorContext constraintValidatorContext) {
            return strings.stream().allMatch(this::isValidPhoneNumber);
        }
    }

    abstract static class AbstractPhoneValidator<T> implements ConstraintValidator<PhoneNumber, T> {
        protected String locale;
        private final PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();

        @Override
        public void initialize(PhoneNumber constraintAnnotation) {
            locale = constraintAnnotation.locale();
        }

        protected boolean isValidPhoneNumber(String phoneNumber) {
            try {
                Phonenumber.PhoneNumber googlePhoneNumber = phoneNumberUtil.parse(phoneNumber, locale);
                return phoneNumberUtil.isValidNumber(googlePhoneNumber) &&
                        phoneNumberUtil.isPossibleNumber(googlePhoneNumber) &&
                        phoneNumberUtil.isPossibleNumberForType(googlePhoneNumber, MOBILE);
            } catch (NumberParseException ex) {
                return false;
            }
        }
    }
}
