package com.mwallet.mwalletcommons.annotations;

import org.springframework.core.annotation.AliasFor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RestController;

import java.lang.annotation.*;

/**
 * @author lawrence
 * created 19/11/2019 at 15:45
 **/
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@RestController
@Validated
public @interface WebAdapter {
    @AliasFor(annotation = RestController.class)
    String value() default "";
}
