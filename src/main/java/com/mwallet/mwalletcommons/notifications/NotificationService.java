package com.mwallet.mwalletcommons.notifications;

import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;

/**
 * @author lawrence
 * created 10/20/19 at 3:28 PM
 * <p>
 * This interface defines a base contract for all services that offer some kind of notifications sourcing.
 * It is required that such a service will assemble and produce a Notification object to the message broker.
 **/

@Service
@Validated
public interface NotificationService<T> {

    /**
     * Assemble a Notification object extracting details from the sourceObject and attach a notificationType
     * to allow respective handling by the notification service
     *
     * @param sourceObject     POJO containing required fields
     * @param notificationState notification state
     * @param notificationType type of notification
     * @param recipientEgressTypes map of recipients and their preferred egress types
     */
    void sendNotification(
            @Valid T sourceObject,
            @Valid @NotNull NotificationState notificationState,
            @Valid @NotNull NotificationType notificationType,
            @Valid @NotNull HashMap<Notification.Link, List<EgressType>> recipientEgressTypes);

}
