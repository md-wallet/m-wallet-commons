package com.mwallet.mwalletcommons.notifications;

/**
 * @author lawrence
 * This enum captures various notification channels
 */
public enum EgressType {
    SLACK,
    APP,
    SMS,
    EMAIL,
    TELEGRAM
}
