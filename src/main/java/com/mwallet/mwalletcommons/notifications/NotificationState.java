package com.mwallet.mwalletcommons.notifications;

/**
 * This enum models the state of business POJO and guides the logic
 * of deciding whether the POJO should trigger a notification or not
 */
public enum NotificationState {
    NONE,
    ALARM,
    ALERT,
}
