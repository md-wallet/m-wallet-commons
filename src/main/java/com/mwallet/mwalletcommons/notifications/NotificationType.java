package com.mwallet.mwalletcommons.notifications;

/**
 * @author lawrence
 * This enum captures various notification types
 */
public enum NotificationType {
    OTP,
    TRANSACTION_SUCCESS,
    TRANSACTION_FAIL,
    TRANSACTION_DUPLICATE
}
