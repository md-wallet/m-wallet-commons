package com.mwallet.mwalletcommons.notifications;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.KeyDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

import static com.mwallet.mwalletcommons.utils.ObjectUtils.readJson;
import static com.mwallet.mwalletcommons.utils.ObjectUtils.writeJson;


@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder(toBuilder = true)
@EqualsAndHashCode
public class Notification {
    @NotNull
    private NotificationType notificationType;

    @NotNull
    @JsonDeserialize(keyUsing = LinkKeyDeserializer.class)
    private HashMap<Link, List<EgressType>> recipientEgressTypes;

    @NotEmpty
    private HashMap<String, Object> customFields;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    @NotNull
    private LocalDateTime timestamp;

    public enum LinkType {
        USER,
        TRUST
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @SuperBuilder(toBuilder = true)
    @EqualsAndHashCode
    public static class Link {
        String linkId;
        LinkType linkType;

        @Override
        public String toString() {
            return writeJson(this);
        }
    }


    static class LinkKeyDeserializer extends KeyDeserializer {
        @Override
        public Object deserializeKey(String s, DeserializationContext deserializationContext) throws IOException {
            return readJson(s, Link.class);
        }
    }
}
