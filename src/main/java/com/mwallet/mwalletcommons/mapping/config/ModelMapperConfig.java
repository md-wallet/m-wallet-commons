package com.mwallet.mwalletcommons.mapping.config;

import com.github.rozidan.springboot.modelmapper.ConfigurationConfigurer;
import org.modelmapper.Conditions;
import org.modelmapper.config.Configuration;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

/**
 * @author lawrence
 * created 07/11/2019 at 00:58
 **/
@Component
public class ModelMapperConfig extends ConfigurationConfigurer {
    @Override
    public void configure(Configuration configuration) {
        configuration.setMatchingStrategy(MatchingStrategies.STRICT);
        configuration.setPropertyCondition(Conditions.isNotNull());
        configuration.setSkipNullEnabled(true);
    }
}
