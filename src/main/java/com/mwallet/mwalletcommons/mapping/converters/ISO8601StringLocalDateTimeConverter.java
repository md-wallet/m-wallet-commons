package com.mwallet.mwalletcommons.mapping.converters;

import com.github.rozidan.springboot.modelmapper.ConverterConfigurer;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author lawrence
 * created 14/01/2020 at 22:26
 **/
@Component
public class ISO8601StringLocalDateTimeConverter extends ConverterConfigurer<String, LocalDateTime> {
    @Override
    public Converter<String, LocalDateTime> converter() {
        return new AbstractConverter<String, LocalDateTime>() {
            @Override
            protected LocalDateTime convert(String s) {
                return LocalDateTime.parse(s,DateTimeFormatter.ISO_DATE_TIME);
            }
        };
    }
}
