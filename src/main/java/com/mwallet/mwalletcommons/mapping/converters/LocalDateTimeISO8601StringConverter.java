package com.mwallet.mwalletcommons.mapping.converters;

import com.github.rozidan.springboot.modelmapper.ConverterConfigurer;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author lawrence
 * created 14/01/2020 at 22:24
 **/
@Component
public class LocalDateTimeISO8601StringConverter extends ConverterConfigurer<LocalDateTime, String> {
    @Override
    public Converter<LocalDateTime, String> converter() {
        return new AbstractConverter<>() {
            @Override
            protected String convert(LocalDateTime localDateTime) {
                return DateTimeFormatter.ISO_DATE_TIME.format(localDateTime);
            }
        };
    }
}
