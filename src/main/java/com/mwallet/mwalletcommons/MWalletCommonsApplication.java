package com.mwallet.mwalletcommons;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MWalletCommonsApplication {

    public static void main(String[] args) {
        SpringApplication.run(MWalletCommonsApplication.class, args);
    }

}
