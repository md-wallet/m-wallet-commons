package com.mwallet.mwalletcommons.utils;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.binding.BinderAwareChannelResolver;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.function.Predicate;

/**
 * @author lawrence
 * created 04/02/2020 at 01:46
 **/
@Slf4j
@Service
@Validated
@RequiredArgsConstructor
public class MessageSender {

    private final BinderAwareChannelResolver binderAwareChannelResolver;

    public <T> boolean send(Message<T> message, String destination, Long timeout, Predicate<Message<T>> send) {
        if (send.test(message))
            return binderAwareChannelResolver.resolveDestination(destination).send(message);
        else
            return false;
    }

    public <T> boolean send(Message<T> message, String destination, Predicate<Message<T>> send) {
        if (send.test(message))
            return binderAwareChannelResolver.resolveDestination(destination).send(message);
        else
            return false;
    }

    public <T> boolean send(Message<T> message, Long timeout, String destination) {
        return send(message, destination, timeout, pred -> true);
    }

    public <T> boolean send(Message<T> message, String destination) {
        return send(message, destination, pred -> true);
    }


    public <T> boolean send(Object object, Long timeout, String destination) {
        return send(AMQPUtils.buildMessageFrom(object), timeout, destination);
    }

    public <T> boolean send(Object object, String destination) {
        return send(AMQPUtils.buildMessageFrom(object), destination);
    }

    public boolean sendDelayed(DelayedMessage<?> delayedMessage, String destination) {
        Long retries = delayedMessage.retryCount == null ? 1 : delayedMessage.retryCount;
        Long delay = delayedMessage.delay == null ? 2000 : delayedMessage.delay;
        Long timeout = delayedMessage.timeout == null ? 2000 : delayedMessage.timeout;

        HashMap<String, Object> headers = new HashMap<>() {{
            put("x-retries", retries);
            put("x-delay", delay);
            put("x-return", delayedMessage.address);
        }};
        return send(AMQPUtils.buildMessageFrom(delayedMessage.getPayload(), headers), timeout, destination);
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder(toBuilder = true)
    public static class DelayedMessage<T> {
        private Long delay;
        private Long retryCount;
        private Long timeout;

        @NotBlank
        private String address;
        @NotNull
        private T payload;
    }
}
