package com.mwallet.mwalletcommons.utils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;

/**
 * @author lawrence
 * created 19/12/2019 at 21:03
 **/
public class DateUtils {
    /**
     * Get current zonedDateTime at specified zone eg Africa/Nairobi
     *
     * @param zoneId id of zone
     * @return current zonedDateTime
     */
    public static ZonedDateTime getZonedDateTimeNow(String zoneId) {
        return LocalDateTime.now()
                .atZone(ZoneId.of(zoneId));
    }

    /**
     * Get zonedDateTime as at hourOfDay(0-23) referenced by Today
     * at specified zone eg Africa/Nairobi
     *
     * @param zoneId    id of zone
     * @param hourOfDay hour of day
     * @return zonedDateTime at hourOfDay
     */
    public static ZonedDateTime getZonedDateTimeAtHourOfDay(String zoneId, int hourOfDay) {
        return LocalDateTime.now()
                .atZone(ZoneId.of(zoneId))
                .with(ChronoField.HOUR_OF_DAY, hourOfDay);
    }
}
