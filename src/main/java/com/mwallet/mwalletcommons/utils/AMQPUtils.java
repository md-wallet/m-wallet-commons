package com.mwallet.mwalletcommons.utils;


import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;

import java.util.List;
import java.util.Map;

/**
 * @author lawrence
 * created 2019-08-05 at 15:16
 **/
public class AMQPUtils {

    public static Long getDeadLetterCount(Map<?, ?> deathHeader) {
        if (deathHeader == null) return null;

        return (Long) deathHeader.get("count");
    }

    public static Long getDeadLetterCount(Object deathHeader) {
        if (deathHeader == null) return null;

        Map<?, ?> death;
        if (deathHeader instanceof Map) {
            death = (Map<?, ?>) deathHeader;
        } else {
            death = (Map<?, ?>) ((List<?>) deathHeader).get(0);
        }
        return (Long) death.get("count");
    }

    public static <T> Message<T> buildMessageFrom(T var) {
        return MessageBuilder.withPayload(var).build();
    }

    public static <T> Message<T> buildMessageFrom(T var, Map<String, Object> headers) {
        return MessageBuilder
                .withPayload(var)
                .copyHeaders(headers)
                .build();
    }

    public static <T> Message<T> buildMessageFrom(T var, String headerName, Object header) {
        return MessageBuilder
                .withPayload(var)
                .setHeaderIfAbsent(headerName, header)
                .build();
    }

    /**
     * Calculate message delay based on the current failed attempts to process the message
     * and a given maximum delay
     *
     * @param failedAttempts retries counter
     * @param maxDelay       maximum delay
     * @return calculated delay
     */
    public static long exponentialDelay(long failedAttempts, long maxDelay) {
        Double delay = ((1d / 2d) * (Math.pow(2d, failedAttempts) - 1d));
        return Math.min(delay.longValue(), maxDelay);
    }
}
