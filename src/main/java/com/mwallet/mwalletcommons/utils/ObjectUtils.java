package com.mwallet.mwalletcommons.utils;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.SneakyThrows;

import java.security.SecureRandom;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author lawrence
 * created 10/28/19 at 4:41 PM
 **/
public class ObjectUtils {
    public static final DateTimeFormatter DF;
    private final static ObjectMapper jsonMapper;
    private final static ObjectMapper xmlMapper;

    static {
        jsonMapper = new ObjectMapper();
        jsonMapper.enable(SerializationFeature.INDENT_OUTPUT);
        jsonMapper.registerModules(new JavaTimeModule());
        jsonMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        jsonMapper.enable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING);
        jsonMapper.enable(DeserializationFeature.READ_ENUMS_USING_TO_STRING);
        jsonMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        jsonMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        DF = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");


        JacksonXmlModule xmlModule = new JacksonXmlModule();
        xmlModule.setDefaultUseWrapper(false);
        xmlMapper = new XmlMapper(xmlModule);
        xmlMapper.enable(SerializationFeature.INDENT_OUTPUT);
        xmlMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        xmlMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
    }

    /**
     * Serializes object to json
     *
     * @param object object to serialize
     * @return json object
     */
    @SneakyThrows
    public static String writeJson(Object object) {
        return jsonMapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
    }

    /**
     * Serialize object to json ignore all null fields
     *
     * @param object object to serialize
     * @return json object
     */
    @SneakyThrows
    public static String writeJsonIgnoreNull(Object object) {
        jsonMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        String json = jsonMapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
        jsonMapper.setSerializationInclusion(JsonInclude.Include.USE_DEFAULTS);
        return json;
    }

    /**
     * Deserialize json object to a java pojo
     *
     * @param jsonString Json object to deserialize
     * @param valueType  expected Object type
     * @param <T>        Class type
     * @return java pojo
     */
    @SneakyThrows
    public static <T> T readJson(String jsonString, Class<T> valueType) {
        return jsonMapper.readValue(jsonString, valueType);
    }

    /**
     * Get random ENUM value
     *
     * @param clazz target enum class
     * @param <T>   class type
     * @return found enum
     */
    public static <T extends Enum<?>> T randomEnum(Class<T> clazz) {
        SecureRandom random = new SecureRandom();
        int x = random.nextInt(clazz.getEnumConstants().length);
        return clazz.getEnumConstants()[x];
    }

    /**
     * Serialize java object to XML string
     *
     * @param object object to serialize
     * @return resulting XML string
     */
    @SneakyThrows
    public static String writeXml(Object object) {
        return xmlMapper.writeValueAsString(object);
    }

    /**
     * Deserialize xml string to Java object
     *
     * @param xmlString xml string
     * @param clazz     class
     * @param <T>       generic type
     * @return deserialized object
     */
    @SneakyThrows
    public static <T> T readXml(String xmlString, Class<T> clazz) {
        return xmlMapper.readValue(xmlString, clazz);
    }

    /**
     * Get random enum value excluding provided list
     *
     * @param clazz   target enum class
     * @param exclude list of enums to exclude
     * @param <T>     class type
     * @return found enum
     */
    @SafeVarargs
    public static <T extends Enum<?>> T randomEnumExcluding(Class<T> clazz, T... exclude) {
        List<T> toExclude = Arrays.asList(exclude);
        List<T> toInclude = Arrays.stream(clazz.getEnumConstants())
                .filter(toExclude::contains)
                .collect(Collectors.toList());

        return toInclude.get(new SecureRandom().nextInt(toInclude.size()));
    }


}
