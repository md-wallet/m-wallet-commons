package com.mwallet.mwalletcommons.utils;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.AuthenticatedPrincipal;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author lawrence
 * created 11/03/2020 at 21:14
 **/
@Slf4j
@NoArgsConstructor
@SuppressWarnings({"unchecked", "rawtypes"})
public class CustomAuthenticationConverter implements Converter<Jwt, AbstractAuthenticationToken> {

    public AbstractAuthenticationToken convert(Jwt jwt) {
        Collection<String> authorities = (Collection) jwt.getClaims().get("authorities");
        Collection<String> scopes = (Collection) jwt.getClaims().get("scope");
        Stream<GrantedAuthority> roleAuthorities = Stream.empty();
        Stream<GrantedAuthority> roleScopes = Stream.empty();

        if (authorities != null && !authorities.isEmpty()) {
            roleAuthorities = authorities.stream().map((role) -> {
                if (!role.startsWith("ROLE"))
                    role = "ROLE_" + role;
                return role;
            }).map(SimpleGrantedAuthority::new);
        }

        if (scopes != null && !scopes.isEmpty()) {
            roleScopes = scopes.stream().map((scope) -> "SCOPE_" + scope).map(SimpleGrantedAuthority::new);
        }

        Collection<GrantedAuthority> allAuthorities = Stream.concat(roleAuthorities, roleScopes).collect(Collectors.toList());
        AbstractAuthenticationToken authenticationToken = new AbstractAuthenticationToken(allAuthorities) {
            public Object getCredentials() {
                return jwt;
            }

            public Object getPrincipal() {
                return (AuthenticatedPrincipal) () -> getPrincipalName(jwt);
            }
        };
        authenticationToken.setAuthenticated(true);
        return authenticationToken;
    }

    private String getPrincipalName(Jwt jwt){
        if(jwt.getClaims().containsKey("user_name"))
            return (String) jwt.getClaims().get("user_name");
        else
            return (String) jwt.getClaims().get("client_id");
    }
}
