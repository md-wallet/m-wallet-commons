package com.mwallet.mwalletcommons.utils;

import lombok.experimental.UtilityClass;

import java.math.RoundingMode;

/**
 * @author lawrencemwaniki
 * created 29/04/2020 at 19:02
 **/
@UtilityClass
public class MathUtils {

    public int SCALE = 0;
    public RoundingMode ROUNDING_MODE = RoundingMode.CEILING;
}
