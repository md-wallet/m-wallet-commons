package com.mwallet.mwalletcommons.utils;

import lombok.SneakyThrows;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.util.Assert;

import java.util.UUID;

/**
 * @author lawrence
 * created 18/01/2020 at 20:46
 **/
@SuppressWarnings("unchecked")
public final class SecurityUtils {


    /**
     * Gets extra token info. This is the info stored in the
     * token claims.
     *
     * @param key  info name
     * @param type type of info to get
     * @param <T>  class type
     * @return found info
     */
    @SneakyThrows
    public static <T> T getExtraInfo(String key, Class<T> type) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Assert.notNull(auth, "Authentication cannot be null");
        Jwt claims = (Jwt) auth.getCredentials();
        return (T) claims.getClaims().get(key);
    }

    /**
     * Generate uuid
     *
     * @return uuid
     */
    public static String generateUUID() {
        return UUID.randomUUID().toString();
    }
}
