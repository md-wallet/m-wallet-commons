package com.mwallet.mwalletcommons.exceptions;

import com.netflix.hystrix.exception.HystrixBadRequestException;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


/**
 * @author lawrence
 * This class centralizes our exception handling in that it interprets our {@link GenericHttpException}
 * checks the RESPONSE_CODE and gives useful info to the API user
 */
@Slf4j
@Order(Ordered.HIGHEST_PRECEDENCE)
@RestControllerAdvice //combines controllerAdvice & ResponseBody for easy bootstrapping with http
public class GlobalExceptionHandlerController {

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        StringTrimmerEditor stringTrimmer = new StringTrimmerEditor(true);
        binder.registerCustomEditor(String.class, stringTrimmer);
    }

    /**
     * on authentication fail inform user as clearly as possible - deny
     *
     * @param res Response Object
     * @throws IOException we are doing http so we expect something may go wrong
     */
    @ExceptionHandler(AccessDeniedException.class)
    public void handleAccessDeniedException(HttpServletResponse res) throws IOException {
        res.sendError(HttpStatus.FORBIDDEN.value(), "Access denied");
    }


    /**
     * if user input doesnt match our expected Data Transfer Objects then inform them
     * clearly too
     *
     * @param res Response Object
     * @param e   Exception thrown
     * @throws IOException we are doing http so we expect something may go wrong
     */
    @ExceptionHandler(Exception.class)
    public void handleException(HttpServletResponse res, Exception e) throws IOException {
        log.error(e.getClass().getSimpleName(), e);
        res.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Internal Server Error");
    }

    @ExceptionHandler(HystrixBadRequestException.class)
    public void handleHystrixBadRequestException(HttpServletResponse res, HystrixBadRequestException ex) throws IOException {
        log.error("HystrixBadRequestException: ", ex);
        if (ex.getCause() instanceof GenericHttpException) {
            GenericHttpException genericHttpException = (GenericHttpException) ex.getCause();
            handleGenericHttpException(res, genericHttpException);
        } else
            res.sendError(HttpStatus.BAD_REQUEST.value(), ex.getMessage());
    }

    /**
     * sometimes errors are thrown and we dont know what went wrong
     * we will keep this during dev stage, not ideal for prod
     *
     * @param res Response Object
     * @param ex  Custom Exception handle
     */
    @SneakyThrows
    @ExceptionHandler(GenericHttpException.class)
    public void handleGenericHttpException(HttpServletResponse res, GenericHttpException ex) {
        log.error("GenericHttpException: ", ex);
        res.sendError(ex.getHttpStatus().value(), ex.getMessage());
    }


    /**
     * this method will clean up constraint violation errors and return an aggregated error string
     *
     * @param res Response object
     * @param ce  constraint violation exception
     */
    @SneakyThrows
    @ExceptionHandler(ConstraintViolationException.class)
    public void handleConstraintViolationException(HttpServletResponse res, ConstraintViolationException ce) {
        log.error("ConstraintViolationException: ", ce);

        Map<String, String> errors = new HashMap<>();
        ce.getConstraintViolations().forEach(constraintViolation -> {
            String fieldName = constraintViolation.getPropertyPath().toString();
            String errorMessage = constraintViolation.getMessage();
            errors.put(fieldName, errorMessage);
        });
        res.sendError(HttpStatus.BAD_REQUEST.value(), errors.toString());
    }

    @SneakyThrows
    @ExceptionHandler({MethodArgumentNotValidException.class})
    public void handleValidationExceptions(HttpServletResponse res, MethodArgumentNotValidException ex) {
        log.error("MethodArgumentNotValidException: ", ex);

        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        res.sendError(HttpStatus.BAD_REQUEST.value(), errors.toString());
    }

}
