package com.mwallet.mwalletcommons.exceptions;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

/**
 * @author lawrence
 * created 10/28/19 at 1:38 PM
 **/
@Getter
@Setter
public class GenericHttpException extends RuntimeException {
    private final HttpStatus httpStatus;

    public GenericHttpException(String message, Throwable cause, HttpStatus httpStatus) {
        super(message, cause);
        this.httpStatus = httpStatus;
    }

    public GenericHttpException(String message, HttpStatus httpStatus) {
        this(message, null, httpStatus);
    }

    public GenericHttpException(HttpStatus httpStatus) {
        this(null, httpStatus);
    }
}
