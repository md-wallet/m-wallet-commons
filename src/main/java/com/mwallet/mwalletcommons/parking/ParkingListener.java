package com.mwallet.mwalletcommons.parking;


import com.mwallet.mwalletcommons.utils.AMQPUtils;
import com.mwallet.mwalletcommons.utils.MessageSender;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.stereotype.Service;

import java.util.HashMap;

import static com.mwallet.mwalletcommons.utils.ObjectUtils.writeJson;


/**
 * @author lawrence
 * created 04/02/2020 at 01:01
 **/
@Slf4j
@Service
@RequiredArgsConstructor
@EnableBinding({ParkingSource.class, ParkingSink.class})
class ParkingListener {

    private final MessageSender messageSender;

    @StreamListener(target = ParkingSink.PARKING_INPUT)
    public void receiveParkedMessage(Object object, @Headers MessageHeaders messageHeaders) {

        Long retryCount = messageHeaders.get("x-retries", Long.class);
        Integer delay = messageHeaders.get("amqp_receivedDelay", Integer.class);
        String address = messageHeaders.get("x-return", String.class);

        log.info("===================== ParkingListener ====================");
        log.debug("parkedMessage: {}", writeJson(object));
        log.info("retryCount: {}", retryCount);
        log.info("delay: {}", delay);
        log.info("address: {}", address);
        log.info("===========================================================");

        if (retryCount == null) retryCount = 1L;
        HashMap<String,Object> returnHeaders = new HashMap<>(messageHeaders);
        returnHeaders.put("x-retries", retryCount + 1);

        messageSender.send(AMQPUtils.buildMessageFrom(object,returnHeaders),address);
    }

}
