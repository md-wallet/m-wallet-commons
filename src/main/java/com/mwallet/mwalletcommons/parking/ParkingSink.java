package com.mwallet.mwalletcommons.parking;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

/**
 * @author lawrence
 * created 04/02/2020 at 00:59
 **/
public interface ParkingSink {
    String PARKING_INPUT = "parkingChannelInput";

    @Input(ParkingSink.PARKING_INPUT)
    SubscribableChannel parkingInput();
}
