package com.mwallet.mwalletcommons.parking;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

/**
 * @author lawrence
 * created 04/02/2020 at 00:57
 **/
public interface ParkingSource {
    String PARKING_OUTPUT = "parkingChannelOutput";

    @Output(ParkingSource.PARKING_OUTPUT)
    MessageChannel parkingOutput();
}
