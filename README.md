# MWallet Commons

![pipeline](https://gitlab.com/md-wallet/m-wallet-commons/badges/master/pipeline.svg)

MWallet Commons, a package of utility classes considered standard
and useful across all MWallet microservices to justify shared dependency.

## Concerns

This Library addresses:
* ### Annotations
    * `DDD` inspired annotations eg `@UseCase, @PersistenceAdapter` etc for easier documentation
    * Validation eg `@PhoneNumber` for validating phone numbers using Google's libphonenumber library
    * Autoconfiguration of this library using `@EnableMWalletCommons`
* ### Data
    * Loading `YAML` files as default property configurations
    * Standard `BaseEntity` & `BaseDomain` classes to provide metadata to entity & domain objects respectively 
    * `GenericPersistenceAdapter` that utilizes Spring's Example API for query free data manipulation
    * `IDGenerator` for masking auto-generated database ids using hash-ids
      
* ### Exceptions
    * `GenericHttpException` and `GlobalExceptionHandler` to standardize exception handling across the Controller Layer
* ### Utils
    * `ObjectUtils` - parsing and writing JSON/XML            
    * `AMQPUtils` - interacting with RabbitMQ - exponentialDelay, deadLetterCount, serialization
    * `ComparableUtils` - boolean wrapper for any class that implements Comparable
     
* ### Mapping
    * Addresses object mapping using Modelmapper - converters, providers, typeMaps etc
      
## Distribution Management

This is a Spring Boot project and as such uses `maven` build and deploy 
automation. You can download source and binaries from this repository. Alternatively and preferably you should pull it from the `Maven Repository`

* Dependency: 
    ```xml
        <dependency>
            <groupId>com.mwallet</groupId>
            <artifactId>m-wallet-commons</artifactId>
            <version>0.0.1-SNAPSHOT</version>
        </dependency>
    ```

* Repository: 

    ```xml
      <repositories>
        ---
        <repository>
            <id>myMavenRepo.read</id>
            <url>https://mymavenrepo.com/repo/heDg12ThINsXIFDYvKVI/</url>
        </repository>  
        ---  
      </repositories>
    ```
## Copyright

* Copyright (C) Lawrence Mwaniki - All Rights Reserved
 * Unauthorized copying of this project, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Lawrence Mwaniki <kilailawrence@gmail.com>, May 2020
 

   
       